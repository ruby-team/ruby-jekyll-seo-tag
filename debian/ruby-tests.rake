require 'gem2deb/rake/spectask'

ENV['LC_ALL'] = 'C.UTF-8' # fix invalid byte sequence in US-ASCII (ArgumentError)

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
end
